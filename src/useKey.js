import { useEffect } from "react";

export function UseKey(key , action){
    
      useEffect(
        function(){
          function callback(e){
          if (e.code.toLowerCase() === key.toLowerCase()){
            action()
            console.log('escape closing');
        }
      }
        document.addEventListener("keydown", callback);
        return () => {
        document.removeEventListener("keydown", callback);
        }
      }
      ,[action , key]);
}