import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
//import StartRating from './StarRating';


// function Test(){
//   const [test,onTest] = useState(0)
//   return (
//     <div>
//        <StartRating maxRating={7}  color='green' setStars={onTest}/>
//        <p>This element will render {test} starts</p>
//     </div>
//   )
// }

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
    <App />
    // {/* <StartRating maxRating={5} className="test" 
    // messages= { ["terrible" , "bad" , "good" , "great" , "amazing"] }
    
    // />
    // <Test/> */}
  // </React.StrictMode>
);

