import { useEffect, useRef, useState } from "react";
import StartRating from "./StarRating";
import { useMovies } from "./useMovies";
import { useLocalStorageState } from "./useLocalStorageState";
import { UseKey } from "./useKey";
const average = (arr) => {
  const filteredArr = arr.filter((value) => !isNaN(value));
  return filteredArr.length > 0 ? filteredArr.reduce((acc, cur) => acc + cur, 0) / filteredArr.length : 0;
};
  const KEY = "e21959ec";
export default function App() {
  const [query, setQuery] = useState("");
  const [selectedId, setSelectedId]=useState(null);
  //how can i save my data after each render using usestate , so in this way my data still there and not remove ? answer:using call back function in usestate.
  // const [watched, setWatched] = useState([]);

  const {movies,isLoading,error} = useMovies(query)

  const [watched , setWatched] = useLocalStorageState([] , "watched")

  function handleSelectedId(id){
    setSelectedId((selectedId)=>(id === selectedId ? null : id ));
  }
  function handleCloseMovie(){
    setSelectedId(null);
  }

  function handleAddWatchedMovie(movie ){
    setWatched((watched)=> ( [...watched , movie]));
    // localStorage.setItem("watched",JSON.stringify([...watched,movie])); that was normal way , but it is better to use useEffect
  }

  function handleDeleteMovie(id){
    setWatched((watched)=> watched.filter((movie)=> movie.imdbID !== id));
  }

  return (
    <>
     <Navbar>
      <Search query={query} setQuery={setQuery}/>
      <NumResult  movies={movies}/>
     </Navbar>
     <Main>
     {/* <Box>
     <MovieList movies={movies}/>
     </Box> 
     <Box>  
     <WatchSummary watched={watched}  />
     <MovieRating watched={watched}/>
     </Box>  */}
     {/* passing elements as a props */}
     <Box element=
     {
     <>
     { isLoading && <Loader/> }
     { error && <ErrorMessage message={error}/>}
     { !isLoading  && !error && <MovieList movies={movies} 
        onDeleteMovie={handleDeleteMovie}
        onSelectdMovie={handleSelectedId}
        />
        }
     </>
     }
     />
     <Box element= 
     {
      selectedId
      ? 
      <MovieDetails 
      selectedId={selectedId} 
      onCloseMovie={handleCloseMovie} 
      onAddWatchMovie={handleAddWatchedMovie}
      watched={watched}
      />
      :
      <> 
       <WatchSummary watched={watched}/>
       <MovieRating watched={watched} onDeleteMovie={handleDeleteMovie}/>
       
       </> 
      
      }
      
     />  
    
     

     </Main>
      </>   
          )}
  function Navbar({children}){
    return (
      <div>
         <nav className="nav-bar">
         <Logo/>
          {children}
      </nav>
      </div>
    )
    }

function Loader(){
  return (
    
      <p className="loader">is Loading ..... ⏳ </p>
    
  )
}

function ErrorMessage({message}){
  return (
    <p className="error">
      <span>⭐️⭐️⭐️</span>{message}
    </p>

  )
}
function NumResult({movies}){
return(
  <div>
    <p className="num-results">
          Found <strong>{movies.length}</strong> results
    </p>
  </div>
)
}
    function Search({query , setQuery}){

      const inputEl = useRef(null);

      UseKey('Enter',  function () {
        if(document.activeElement === inputEl.current) return;
        inputEl.current.focus();
        setQuery("")    
      });
  


    //   this is not react way to deal with this problem , so it is better to use useRef
    //  useEffect(()=>{
    //   const el = document.querySelector(".search");
    //   el.focus();
    //  },[])
      return (
        <div>
             <input
          className="search"
          type="text"
          placeholder="Search movies..."
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          ref={inputEl}
        />
        </div>
      )
    }
    function Logo(){
      return(
           <div className="logo">
          <span role="img">🍿</span>
          <h1>usePopcorn</h1>
        </div>
      )
    }
    function Main({children}){ 
      return (
        <div className="main">
          {children}
        </div>
      )
    }

    function MovieList({movies , onSelectdMovie  }){
     
      return(
          <div>
          <ul className="list list-movies">
            {movies?.map((movie) => (
              <Movie movie={movie} key={movie.imdbID} onSelectdMovie={onSelectdMovie} />
              ))}
          </ul>
          </div>
          )}

  function Movie({movie , onSelectdMovie }){
      return (
        <div>
        <li onClick={()=> onSelectdMovie(movie.imdbID)}>
        <img src={movie.Poster} alt={`${movie.Title} poster`} />
        <h3>{movie.Title}</h3>
        <div>
          <p>
            <span>🗓</span>
            <span>{movie.Year}</span>
          </p>
        </div>
      </li>
  
    </div>
      )
    }
    // function Box({children}){
    //   const [isOpen, setIsOpen] = useState(true);
    //   return(
    //     <div className="box">
    //     <button
    //       className="btn-toggle"
    //       onClick={() => setIsOpen((open) => !open)}
    //     >
    //       {isOpen ? "–" : "+"}
    //     </button>
    //     {isOpen && children }
    //        </div>
    //   );
    // }
    // passing element as a props:
    function Box({element}){
      const [isOpen, setIsOpen] = useState(true);
      return(
        <div className="box">
        <button
          className="btn-toggle"
          onClick={() => setIsOpen((open) => !open)}
        >
          {isOpen ? "–" : "+"}
        </button>
        {isOpen && element }
           </div>
      );
    }
    function WatchSummary({watched}){
      const avgImdbRating = average(watched.map((movie) => movie.imdbRating));
      const avgUserRating = average(watched.map((movie) => movie.userRating));
      const avgRuntime = average(watched.map((movie) => movie.runtime));

   
      return(
        <div className="summary">
        <h2>Movies you watched</h2>
        <div>
          <p>
            <span>#️⃣</span>
            <span>{watched.length} movies</span>
          </p>
          <p>
            <span>⭐️</span>
            <span>{avgImdbRating}</span>
          </p>
          <p>
            <span>🌟</span>
            <span>{avgUserRating}</span>
          </p>
          <p>
            <span>⏳</span>
            <span>{avgRuntime} min</span>
          </p>
        </div>
      </div>
      )
    }
    function MovieDetails({selectedId ,onCloseMovie, onAddWatchMovie , watched}){
      const [movie , setMovie]= useState({});
      const [isLoading, setIsLoading] = useState(false);
      const [userRating, setUserRating]= useState("");

      const isWatched = watched.map((movie)=>movie.imdbID).includes(selectedId);
      const watchUserRating = watched.find(movie=>movie.imdbID === selectedId)?.userRating;
      
      const countRef = useRef(0);

      useEffect(()=>{
        if (userRating) countRef.current++
      },
      [userRating])

      
      const {
        Title: title,
        Year: year,
        Poster: poster,
        Runtime: runtime,
        imdbRating,
        Plot:plot,
        Released:released,
        Actors:actors,
        Genre:genre,
        Director:director,
      } = movie;
     
      // const [isTop,setIsTop]=useState(imdbRating > 3);
      // console.log(isTop);

      // useEffect(function(){
      //   setIsTop(imdbRating>3)
      // },
      // [imdbRating])

      const [avgRating,setAvgRating]= useState(0)
      function handleAdd(){
        const newWatchedMovie = {
          imdbID:selectedId,
          title,
          year,
          poster,
          imdbRating:Number(imdbRating),
          runtime:Number(runtime.split("").at(0)),
          userRating,
          countRatingDecisions:countRef.current,
        };
        onAddWatchMovie(newWatchedMovie);
        setAvgRating(Number(imdbRating));
        setAvgRating((avgRating)=>(avgRating + userRating)/2);
        onCloseMovie();
      }

      useEffect(()=> {
        async function getMovieDetails()
        {
          setIsLoading(true);
          const res = await fetch(`http://www.omdbapi.com/?apikey=${KEY}&i=${selectedId}`);
          const data = await res.json();
          setMovie(data);
          setIsLoading(false)
        }
        getMovieDetails();
      },[selectedId])

      useEffect( ()=> {
        if(!title) return;
        document.title=`Movie | ${title}`;
        return () => {
          document.title = "usePopCorn";
        }
      },[title])

      UseKey("Escape", onCloseMovie);

      return (
      <div className="details">
        {
        isLoading ? <Loader/> :
        <>
        <header>
        <button className="btn-back" onClick={onCloseMovie}>
          &larr;
        </button>
        <img src={poster} alt={`Poster of ${movie} movie`}/>
        <div className="details-overview">
          <h2>{title}</h2>
          <p>{released} &bull; {runtime}</p>
          <p>{genre}</p>
          <p><span>⭐️</span>{imdbRating} IMDb rating</p>
        </div>
        </header>
        <section>
        <div className="rating">
          {!isWatched ? (
          <>
          <StartRating maxRating={10} size={24} setStars={setUserRating}/>
          {userRating > 0 && (<button className="btn-add" onClick={handleAdd}>+ Add to list</button>)}
          </> ): ( <p>you already rated this movie {watchUserRating}  <span>🌟</span></p> )
          }
        </div>
          <p>
            <em>{plot}</em>
          </p>
          <p>Starring {actors}</p>
          <p>Directed by {director}</p>
          <p>{avgRating}</p>
        </section>
        </>
    }
      </div>

   ) }
    function WatchedMovie({movie ,onDeleteMovie}){
      return (
        <div>
              <li>
                    <img src={movie.poster} alt={`${movie.title} poster`} />
                    <h3>{movie.title}</h3>
                    <div>
                      <p>
                        <span>⭐️</span>
                        <span>{movie.imdbRating}</span>
                      </p>
                      <p>
                        <span>🌟</span>
                        <span>{movie.userRating}</span>
                      </p>
                      <p>
                        <span>⏳</span>
                        <span>{movie.runtime} min</span>
                      </p>
                      <button className="btn-delete" onClick={()=> onDeleteMovie(movie.imdbID)}>X</button>
                    </div>
                  </li>
        </div>
      ) 
    }
    function MovieRating({watched ,onDeleteMovie}){
      return(
        <div>
        <ul className="list">
                {watched.map((movie) => (
                  <WatchedMovie movie={movie} key={movie.imdbID} onDeleteMovie={onDeleteMovie}/>
              
                ))}
              </ul>
              </div>
      )
    }
