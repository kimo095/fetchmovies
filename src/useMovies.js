import { useEffect,useState } from "react";

export function useMovies(query){
    const [movies, setMovies] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error , setError] =useState("");
    const KEY = "e21959ec";
      useEffect(() => {
        const controller = new AbortController();
        async function fetchMovie(){
            try {
            setIsLoading(true)
            setError("")
          const res = await fetch(`http://www.omdbapi.com/?apikey=${KEY}&s=${query}`,{signal:controller.signal});
    
          if(!res.ok) throw new Error("Something went wrong with your fetching your data");
          const data = await res.json();
          if(data.Response ==='False') throw new Error("Sorry movie not exists");
          setMovies(data.Search);
          setError("")
          }catch (err) {
    
          if(err.name !=="AbortError"){
            setError(err.message)
            }
             
          } finally {
            setIsLoading(false)
          }
        }
        
        if(query.length < 3){
          setMovies([]);
          setError("");
          return;
        }
        // handleCloseMovie();
        fetchMovie();
        return ()=>{
          controller.abort();
        };
      },[query]);

      return {movies,isLoading,error}

}